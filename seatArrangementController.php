<?php
class seatArrangementController
{
    public function __construct(){
        $this->dbg = false;
        $seatMap = array();
        $seats = array();
    }

    /*
     * Checking For Seat State After Arrangement
     * */
    public function getSeatStateAfterTaking ($sides, $lines, $rows, $columns, $seatMap) {
        if($this->dbg)
            print("seatMap:". $seatMap);
        $numberOfNeighbor = 0;
        // check neighbours
        if ($sides - 1 >= 0) {
            if ($seatMap[$sides - 1][$lines]) {
                if($this->dbg)
                    print("!Left");
                $numberOfNeighbor += 1;
            }
        }

        if ($sides + 1 < $rows) {
            if ($seatMap[$sides + 1][$lines]) {
                if($this->dbg)
                    print("!Right". $seatMap[$sides + 1 .",". $lines]);
                $numberOfNeighbor += 1;
            }
        }

        if ($lines - 1 >= 0) {
            if ($seatMap[$sides][$lines - 1]) {
                if($this->dbg)
                    print("!Up");
                $numberOfNeighbor += 1;
            }
        }

        if ($lines + 1 < $columns) {
            if ($seatMap[$sides][$lines + 1]) {
                if($this->dbg)
                    print("!Down");
                $numberOfNeighbor += 1;
            }
        }

        if($this->dbg)
            print("getSeatStateAfterTaking: ". $numberOfNeighbor);
        return $numberOfNeighbor;
    }

    /*
     * Checking For Empty Seat After Arrangement
     * */
    public function emptySeatMap ($rows, $columns) {
        for ($row = 0; $row < $rows; $row++) {
            for ($column = 0; $column < $columns; $column++) {
                $coordinate = $row.",".$column;
                $this->seats[$coordinate] = array('neighbor'=> 0,'x'=> $row, 'y'=> $column);
                $this->seatMap[$row][$column] = 0;
            }
        }
        return $this->seatMap;
    }

    /*
     * Arrange Office Room
     * */
    public function arrangeOfficeRoom ($inputData) {

        $rows = $inputData['rows'];
        $columns = $inputData['columns'];
        $numberOfUsers = $inputData['users'];
        if($this->dbg)
            print("INPUT: ". $rows.",". $columns.",". $numberOfUsers);
        $seatMap = $this->emptySeatMap($rows, $columns);
        if($this->dbg)
            print("seatMap: ". $seatMap);
        if($this->dbg)
            print("seats: ". $this->seats);

        $minNumberOfNeighbor = 5;
        $posX = -1;
        $posY = -1;
        $checkingCounter = 0;
        for ($counter = 0; $counter < $numberOfUsers; $counter++) {
            $minNumberOfNeighbor = 5;

            for ($row = 0; $row < $rows; $row++) {
                for ($column = 0; $column < $columns; $column++) {
                    $coordinate = $row."," .$column;
                    if ($seatMap[$row][$column] == 0) {
                        $checkingCounter += 1;
                        $newNumberOfNeighbor = $this->getSeatStateAfterTaking($row, $column, $rows, $columns, $seatMap);
                        if($this->dbg)
                            print("_check seat[,". $row. ",". $column.",]". $newNumberOfNeighbor.",".$minNumberOfNeighbor);
                        if (false && ($checkingCounter % 2 == 1) && $newNumberOfNeighbor == $minNumberOfNeighbor || $newNumberOfNeighbor < $minNumberOfNeighbor) {
                            $minNumberOfNeighbor = $newNumberOfNeighbor;
                            $posX = $row;
                            $posY = $column;
                            if($this->dbg)
                                print("--> new seat: ". $posX.",". $posY);
                        }
                    }
                }
            }

            $seatMap[$posX][$posY] = 1;
            $seats[$posX . "," . $posY]['neighbor'] = $minNumberOfNeighbor;
            if($this->dbg)
                print("Seat [," .$posX. ",". $posY. ",] has been taken.");
        }

        if($this->dbg)
            print("INPUT: ,". $rows. ",". $columns. ",". $numberOfUsers);
        if($this->dbg)
            print("ANSWER: ");
        if($this->dbg)
            print("seatMap: ,". $seatMap);

        // Final Step: Check Neighbor
        $count = 0;
        for ($row = 0; $row < $rows; $row++) {
            for ($column = 0; $column < $columns; $column++) {
                if ($column + 1 < $columns && $seatMap[$row][$column] && $seatMap[$row][$column + 1]) {
                    $count += 1;
                }
            }
        }

        for ($column = 0; $column < $columns; $column++) {
            for ($row = 0; $row < $rows; $row++) {
                if ($row + 1 < $rows && $seatMap[$row][$column] && $seatMap[$row + 1][$column]) {
                    $count += 1;
                }
            }
        }

        if($this->dbg)
            print("count = ,". $count);

        return $count;
    }
}