<?php
require_once 'seatArrangementController.php';
if(isset($_POST['submit']))
{
    /*
     * Get Input Data From Form
     * */
    $formData = $_POST;

    $objController = new seatArrangementController();
    $output = $objController->arrangeOfficeRoom($formData);
    ?>
    <div align="center">
        <h3>Output is : <?php echo $output; ?></h3><br>
        <a href="index.php">Back To Form</a>
    </div>
    <?php
}else
{
?>
<!DOCTYPE html>
<html>
<head></head>
<body>
<h3 style="text-align: center;">Enter Your Inputs</h3>
<br>
<div align="center">
    <form action="" method="post">
        Rows : <input type="text" name="rows">
        <br><br>
        Columns : <input type="text" name="columns">
        <br><br>
        Users : <input type="text" name="users">
        <br><br>
        <input type="submit" name="submit">
    </form>
</div>
</body>
</html>
    <?php
}
?>